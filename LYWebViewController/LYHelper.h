#//
//  LYHelper.h
//  LYWebViewController
//
//  Created by LvYuan on 16/7/16.
//  Copyright © 2016年 LvYuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LYHelper : NSObject
+ (BOOL)isValidUrl:(NSString *)url;
+ (NSString *)getCompletedUrl:(NSString *)url;
+ (NSString *)getUrlForSearchKeyword:(NSString *)keyword;
@end

//
//  ViewController.m
//  LYWebViewController
//
//  Created by LvYuan on 16/7/9.
//  Copyright © 2016年 LvYuan. All rights reserved.
//

#import "ViewController.h"
#import "LYWebViewController.h"
#import "LYHelper.h"

#define kURL @"https://www.baidu.com"

@interface ViewController ()<UISearchBarDelegate>

@property (nonatomic,strong) LYWebViewController * webVC;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


- (IBAction)searchAction:(id)sender;

@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    self.edgesForExtendedLayout = UIRectEdgeAll;
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}

/**
 *  百度一下
 */
- (void)connect:(NSString *)text {
    _webVC = [[LYWebViewController alloc]init];
    _webVC.url = text;
    [self.navigationController pushViewController:self.webVC animated:true];
}


//搜索动作
- (void)searchAction {
    NSString * text = self.searchBar.text;
    if (text.length) {
        NSString * url = nil;
        if ([LYHelper isValidUrl:text]) {
            url = [LYHelper getCompletedUrl:text];
        }else{
            url = [LYHelper getUrlForSearchKeyword:text];
        }
        [self connect:url];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchAction];
    [self.searchBar resignFirstResponder];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.searchBar resignFirstResponder];
}


@end

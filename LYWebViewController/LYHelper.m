//
//  LYHelper.m
//  LYWebViewController
//
//  Created by LvYuan on 16/7/16.
//  Copyright © 2016年 LvYuan. All rights reserved.
//

#import "LYHelper.h"

@implementation LYHelper

+ (BOOL)isValidUrl:(NSString *)url{
    NSString * urlString = @"^(\\w+:\\/\\/)?\\w+(\\.\\w+)+.*$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlString];
    return [emailTest evaluateWithObject:url];
}

+ (NSString *)getCompletedUrl:(NSString *)url{
    if ([self isValidUrl:url]) {
        if ([url hasPrefix:@"http://"]) {
            return url;
        }
        else if([url hasPrefix:@"https://"]){
            return url;
        }
        else if ([url hasPrefix:@"//"]){
            return [@"http:" stringByAppendingString:url];
        }
        else{
            return [@"http://" stringByAppendingString:url];
        }
        
    }
    return @"";
}

+ (NSString *)getUrlForSearchKeyword:(NSString *)keyword{
    NSString * decode = [keyword stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:@"http://www.baidu.com.cn/s?wd=%@&cl=3",decode];
}

@end


<h1><strong>LYWebViewController</strong></h1>

<ul>
	<li>系统最低支持iOS8.0</li>
	<li>基于WebKit</li>
	<li>支持手势前进 后退</li>
	<li>支持一键关闭</li>
</ul>

<h2><strong>Version</strong></h2>

<p>&nbsp;&nbsp;&nbsp;&nbsp;1.1 build 22</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;新增：添加搜索框，可搜索关键字，也可以直接粘贴链接！</p>

<h2><strong>Usage&nbsp;</strong></h2>

<p>&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp; &nbsp; 将LYWebViewController文件夹拖入到你的工程,在需要的地方引入头文件</p>

<pre>
<code>#import "LYWebViewController.h"</code></pre>

<p>&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp; &nbsp; 创建WebViewController 对象 并 设置 url 值</p>

<pre>
<code>LYWebViewController * webVC = [[LYWebViewController alloc]init];
webVC.url = URL;
[self.navigationController pushViewController:webVC animated:true];</code></pre>

<h3><strong>效果展示</strong></h3>

<p><img alt="" src="http://static.oschina.net/uploads/space/2016/0713/105703_7xtO_2279344.png" /></p>

<p><img alt="" src="http://static.oschina.net/uploads/space/2016/0713/105714_2LCV_2279344.png" /></p>

<h3><strong>注意</strong></h3>

<p>&nbsp;&nbsp;&nbsp;&nbsp;请将webViewController 置于 导航栈中。</p>

<h3><strong>待优化</strong></h3>

<p>&nbsp;&nbsp;&nbsp;&nbsp;内部控件生命周期的优化。</p>
